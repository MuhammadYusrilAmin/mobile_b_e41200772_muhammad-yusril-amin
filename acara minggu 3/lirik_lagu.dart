import 'dart:async';

void main(List<String> args) async {
  print("Lirik Lagu Alan Walker - Lily");
  print("");
  print(await line());
  print(await line2());
  print(await line3());
  print(await line4());
  print(await line5());
  print(await line6());
  print(await line7());
}

Future<String> line() async {
  String greting = "Lily was a little girl";
  return await Future.delayed(Duration(seconds: 4), () => (greting));
}

Future<String> line2() async {
  String greting = "Afraid of the big, wide world";
  return await Future.delayed(Duration(seconds: 3), () => (greting));
}

Future<String> line3() async {
  String greting = "She grew up within her castle walls";
  return await Future.delayed(Duration(seconds: 3), () => (greting));
}

Future<String> line4() async {
  String greting = "Now and then she tried to run";
  return await Future.delayed(Duration(seconds: 3), () => (greting));
}

Future<String> line5() async {
  String greting = "And then on the night with the setting sun";
  return await Future.delayed(Duration(seconds: 3), () => (greting));
}

Future<String> line6() async {
  String greting = "She went in the woods away";
  return await Future.delayed(Duration(seconds: 3), () => (greting));
}

Future<String> line7() async {
  String greting = "So afraid, all alone";
  return await Future.delayed(Duration(seconds: 3), () => (greting));
}
