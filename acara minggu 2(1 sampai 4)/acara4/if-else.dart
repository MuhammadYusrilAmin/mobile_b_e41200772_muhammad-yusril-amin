import 'dart:io';

void main(List<String> args) {
  var nama, peran;
  print('Selamat Datang di aplikasi WereWolf');
  print('Untuk memulai game silahkan masukkan nama dan peran');
  print('Masukkan Nama :');
  nama = stdin.readLineSync()!;
  if (nama == "") {
    print('Nama Harus Di isi!');
  } else {
    print('Pilih peran(penyihir/guard/werewolf) :');
    peran = stdin.readLineSync()!;
    if (peran == "") {
      print('Halo ' + nama + ', Pilih peranmu untuk memulai game!');
    } else if (peran == "penyihir") {
      print('Selamat datang di Dunia Werewolf, ' + nama);
      print('Halo Penyihir ' + nama + ', kamu dapat melihat siapa yang menjadi werewolf!');
    } else if (peran == "guard") {
      print('Selamat datang di Dunia Werewolf, ' + nama);
      print('Halo Guard ' + nama + ', kamu akan membantu melindungi temanmu dari serangan werewolf');
    } else {
      print('Selamat datang di Dunia Werewolf, ' + nama);
      print(
          'Halo Werewolf ' + nama + ', Kamu akan memakan mangsa setiap malam!');
    }
  }
}
