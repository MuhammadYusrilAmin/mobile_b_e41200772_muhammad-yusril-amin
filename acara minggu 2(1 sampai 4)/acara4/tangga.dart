import 'dart:io';

void main(List<String> args) {
  int n = 7;
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n - i - 1; ++j) {
      stdout.write('');
    }
    for (int j = n - i - 1; j < n; ++j) {
      stdout.write('#');
    }
    print(' ');
  }
}
