void main() {
  print("LOOPING PERTAMA");
  var angka = 2;
  while (angka <= 20) {
    print(angka.toString() + ' - I love coding');
    angka += 2;
  }

  print("LOOPING KEDUA");
  var angka2 = 20;
  while (angka2 >= 2) {
    print(angka2.toString() + ' - I will become a mobile developer');
    angka2 -= 2;
  }
}
