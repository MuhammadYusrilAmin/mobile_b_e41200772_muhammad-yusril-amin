void main() {
  // String
  var name = "Muhammad Yusril Amin";
  print(name[9]); // "Y"
  print(name[10]); // "u"
  print(name[11]); // "s"
  print(name[12]); // "r"
  print(name[13]); // "i"
  print(name[14]); // "l"
}
