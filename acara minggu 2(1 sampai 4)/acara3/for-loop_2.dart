void main() {
  // perulangan for-loop mengembalikan nilai angka
  // deklarasi variabel
  var jumlahh = 0;
  for (var deret = 5; deret > 0; deret--) {
    jumlahh += deret; // Menambahkan nilai variable jumlah dengan angka deret
    // print data yang akan di looping
    print('Jumlah saat ini: ' + jumlahh.toString());
  }
  // print data terakhir
  print('Hasil Jumlah Akhir: ' + jumlahh.toString());
}
