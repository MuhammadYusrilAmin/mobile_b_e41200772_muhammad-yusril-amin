class Product {
  final int id;
  final String productName;
  final String productImage;
  final String productDescription;
  final String harga;
  final double price;

  Product(this.id, this.productName, this.productImage, this.productDescription,
      this.harga, this.price);
}
